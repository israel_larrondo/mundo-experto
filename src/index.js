// EXTERNALS
import { library, dom } from '@fortawesome/fontawesome-svg-core';
import {
  faYoutube,
  faTwitter,
  faInstagram,
  faFacebook,
} from '@fortawesome/free-brands-svg-icons/';
import {
  faUserCircle,
  faClipboardList,
  faMapMarkerAlt,
  faQuoteLeft,
  faQuoteRight,
  faBars,
  faChevronRight,
  faCaretRight,
  faSearch,
  faChevronDown
} from '@fortawesome/free-solid-svg-icons/';

library.add(faYoutube, faTwitter, faInstagram, faFacebook);
library.add(
  faUserCircle,
  faClipboardList,
  faMapMarkerAlt,
  faQuoteLeft,
  faQuoteRight,
  faBars,
  faChevronRight,
  faCaretRight,
  faSearch,
  faChevronDown
);
dom.watch();

// STYLES
import './scss/style.scss';

// JS
import {
  // SHOW_PASS,
  // MENU_PRINCIPAL,
  // HEADER_MENUES,
  SLIDER,
  TESTIMONIOS,
  TIENDAS
} from './js/main';

// Invocacion de funciones globales
// SHOW_PASS();
// MENU_PRINCIPAL();
// HEADER_MENUES();
TIENDAS();
SLIDER();
TESTIMONIOS();

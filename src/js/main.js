import Swiper from 'swiper/bundle';
import 'swiper/swiper-bundle.css';
import axios from 'axios';
import { Loader } from '@googlemaps/js-api-loader';

/**
 * Password
 */
export const SHOW_PASS = () => {
  const group = document.querySelectorAll('.show-pass');
  const off = `<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-slash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path d="M13.359 11.238C15.06 9.72 16 8 16 8s-3-5.5-8-5.5a7.028 7.028 0 0 0-2.79.588l.77.771A5.944 5.944 0 0 1 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.134 13.134 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755-.165.165-.337.328-.517.486l.708.709z"/>
                  <path d="M11.297 9.176a3.5 3.5 0 0 0-4.474-4.474l.823.823a2.5 2.5 0 0 1 2.829 2.829l.822.822zm-2.943 1.299l.822.822a3.5 3.5 0 0 1-4.474-4.474l.823.823a2.5 2.5 0 0 0 2.829 2.829z"/>
                  <path d="M3.35 5.47c-.18.16-.353.322-.518.487A13.134 13.134 0 0 0 1.172 8l.195.288c.335.48.83 1.12 1.465 1.755C4.121 11.332 5.881 12.5 8 12.5c.716 0 1.39-.133 2.02-.36l.77.772A7.029 7.029 0 0 1 8 13.5C3 13.5 0 8 0 8s.939-1.721 2.641-3.238l.708.709z"/>
                  <path fill-rule="evenodd" d="M13.646 14.354l-12-12 .708-.708 12 12-.708.708z"/>
                </svg>`;
  const on = `<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.134 13.134 0 0 0 1.66 2.043C4.12 11.332 5.88 12.5 8 12.5c2.12 0 3.879-1.168 5.168-2.457A13.134 13.134 0 0 0 14.828 8a13.133 13.133 0 0 0-1.66-2.043C11.879 4.668 10.119 3.5 8 3.5c-2.12 0-3.879 1.168-5.168 2.457A13.133 13.133 0 0 0 1.172 8z"/>
                <path fill-rule="evenodd" d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
              </svg>`;
  group.forEach((x) => {
    const input = x.querySelector('input');
    const btn = x.querySelector('.show-pass__btn');
    btn.addEventListener('click', (e) => {
      if (input.getAttribute('type') === 'password') {
        input.setAttribute('type', 'text');
        btn.innerHTML = on;
      } else {
        input.setAttribute('type', 'password');
        btn.innerHTML = off;
      }
    });
  });
};

/**
 * Menú principal
 */
export const MENU_PRINCIPAL = () => {
  const btn = document.querySelector('.abrir-menu');
  const menu = document.querySelector('.menu');
  if (btn && menu) {
    btn.addEventListener('click', (e) => {
      e.stopPropagation();
      menu.classList.toggle('menu--active');
    });
    menu.addEventListener('click', (e) => {
      e.stopPropagation();
    });
    document.body.addEventListener('click', (e) => {
      menu.classList.remove('menu--active');
    });
  }
};

/**
 * MenuToggle para los menus del header
 */
export const HEADER_MENUES = () => {
  const btns = document.querySelectorAll('[data-toggle-menu]');
  const menues = document.querySelectorAll('.header__menu');
  // const menuesActivos = document.querySelectorAll('.header__menu--active');
  if (btns.length && menues.length) {
    btns.forEach((x) => {
      x.addEventListener('click', (e) => {
        e.stopPropagation();
        menues.forEach((x) => x.classList.remove('header__menu--active'));
        const menu = document.querySelector(
          e.currentTarget.getAttribute('data-toggle-menu')
        );
        // console.log(menu)
        if (menu) {
          menu.classList.contains('header__menu--active')
            ? menu.classList.remove('header__menu--active')
            : menu.classList.add('header__menu--active');
        }
      });
    });
    document.body.addEventListener('click', (e) => {
      menues.forEach((x) => x.classList.remove('header__menu--active'));
    });
    menues.forEach((x) =>
      x.addEventListener('click', (e) => e.stopPropagation())
    );
  }
};

export const SLIDER = () => {
  const options = {
    autoplay: true,
    loop: true,
    slidesPerView: 1,
    navigation: false,
    pagination: {
      el: '.swiper-pagination',
    },
    breakpoints: {
      992: {
        // navigation: {
        //   nextEl: '.swiper-button-next',
        //   prevEl: '.swiper-button-prev',
        // },
      },
    },
  };
  const swiper = new Swiper('.slider__container', options);
  swiper.init();
};

export const TESTIMONIOS = () => {
  const carousel = document.querySelector('.testimonios__carousel');
  if (carousel) {
    const prev = carousel.querySelector('[data-swiper-prev]');
    const next = carousel.querySelector('[data-swiper-next]');
    const swiper = new Swiper('.testimonios__carousel .swiper-container', {
      loop: true,
      slidesPerView: 3,
      spaceBetween: 20,
      breakpoints: {
        0: {
          slidesPerView: 1,
          pagination: {
            el: '.swiper-pagination',
          },
        },
        992: {
          pagination: false,
          slidesPerView: 3,
        },
      },
      // pagination: {
      //   el: '.swiper-pagination',
      // },

      // Navigation arrows
      // navigation: {
      //   nextEl: '.swiper-button-next',
      //   prevEl: '.swiper-button-prev',
      // },
    });
    prev.addEventListener('click', () => swiper.slidePrev(600));
    next.addEventListener('click', () => swiper.slideNext(600));
  }
};

export const TIENDAS = (zonaDefault) => {
  let map;
  let buscador = document.querySelector('#buscador-tiendas');
  const data = axios.get('/assets/tiendas.json');
  if (buscador) {
    let container = buscador.querySelector('.mapa');
    let zonasSelect = buscador.querySelector('.zonas');
    data.then((response) => {
      // Cargando zonas
      response.data.map((x) => {
        let zona = document.createElement('option');
        zona.value = x.zona;
        zona.innerText = x.zona;
        zonasSelect.append(zona);
        zonasSelect.disabled = false;
      });
      // Ejecutando búsqueda
      const loader = new Loader({
        apiKey: 'AIzaSyCDBZubEDdJH9zXAQpquHrY1yTSX0ufFHE',
        version: 'weekly',
      });
      // Agregar / Actualizar Mapa
      const mostrarMapa = (value) => {
        container.classList.add('mapa--loaded');
        container.innerHTML =
          '<div class="preloader"><div class="preloader__child"></div></div>';
        const zona = response.data.filter((x) => x.zona == value);
        loader.load().then(() => {
          // Creación del mapa
          const map = new google.maps.Map(container, {
            center: {
              lat: zona[0].center[0],
              lng: zona[0].center[1],
            },
            zoom: zona[0].zoom,
          });

          // Información de cada Marker
          zona[0].tiendas.map((x) => {
            const info = `
                  <div id="container" class="mapa__window">
                    <div class="mapa__image">
                      <img src="/assets/tiendas/${x.slug}.jpg">
                    </div>
                    <div class="mapa__window__content">
                      <h5>${x.tienda}</h5>
                      <p><strong>Dirección</strong></p>
                      <div class="mapa__text">${x.info.direccion}</div>
                      <br>
                      <p><strong>Horarios</strong></p>
                      ${
                        x.info.horarios['lun-sab'] &&
                        `<div class="mapa__text">Lunes a Sábado: ${x.info.horarios['lun-sab']}</div>`
                      }
                      ${
                        x.info.horarios['dom-fest'] &&
                        `<div class="mapa__text">Domingos y Festivos: ${x.info.horarios['dom-fest']}</div>`
                      }
                      <p>
                      <br>
                      <strong>Teléfono</strong></p>
                      <div class="mapa__text">${x.info.telefono}</div>
                    </div>
                  </div>
                `;
            const infowindow = new google.maps.InfoWindow({
              content: info,
            });
            const marker = new google.maps.Marker({
              position: { lat: x.coord[0], lng: x.coord[1] },
              map,
              title: x.tienda,
              icon: '/assets/marker.png',
            });
            marker.addListener('click', () => {
              infowindow.open(map, marker);
            });
          });
        });
      };
      zonasSelect.addEventListener('change', (e) => {
        mostrarMapa(e.target.value);
      });

      //  Mostrar RM por defecto
      mostrarMapa('Región Metropolitana');
      zonasSelect.value = 'Región Metropolitana';
    });
  }
};

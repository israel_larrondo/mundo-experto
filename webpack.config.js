const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const BeautifyHtmlWebpackPlugin = require('beautify-html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const dotenv = require('dotenv').config({
  path: path.join(__dirname, '/.env'),
});

// Variables .env
const SITE_NAME = process.env.SITE_NAME;

const HTMLS = {
  index: './src/index.js',
  login: './src/login.js',
  registro: './src/registro.js',
  profile: './src/profile.js',
  'temas-interes': './src/temas-interes.js',
  'temas-interes--single': './src/temas-interes--single.js',
  'preguntas-frecuentes': './src/preguntas-frecuentes.js',
  'horarios-tienda': './src/horarios-tienda.js',
};

const PUG = {
  test: /\.pug$/,
  use: [
    {
      loader: 'string-replace-loader',
      options: {
        multiple: [{ search: /@SITE_NAME@/g, replace: SITE_NAME }],
      },
    },
    {
      loader: 'html-loader',
      options: {
        sources: {
          urlFilter: (attribute, value, resourcePath) => {
            if (/.(png|jpe?g|gif|svg)$/.test(value)) {
              return false;
            }

            return true;
          },
        },
      },
    },
    'pug-html-loader',
  ],
};
const JS = {
  test: /\.m?js$/,
  exclude: /(node_modules|bower_components)/,
  use: {
    loader: 'babel-loader',
    options: {
      presets: ['@babel/preset-env'],
    },
  },
};

const CSS = {
  test: /\.(css|sass|scss)$/i,
  exclude: /(helpers)/,
  use: [
    {
      loader: MiniCssExtractPlugin.loader,
      options: {
        publicPath: '/assets',
      },
    },
    'css-loader',
    'sass-loader',
  ],
};

const ASSETS = {
  test: /\.(jpg|png|gif)$/i,
  use: [{
    loader: 'url-loader'
  }]
}

const config = (mode) => {
  return {
    entry:
      mode === 'production'
        ? {
            main: Object.values(HTMLS),
          }
        : HTMLS,
    output: {
      // path: path.resolve(__dirname, 'dist'),
      filename: '[name].[hash].js',
      clean: true,
      // uniqueName : 'main.[hash]',
      // library: ['MyLibrary', '[name]'],
    },
    devServer: {
      open: true,
      port: 3500,
      compress: true,
      // contentBase: 'dist',
    },
    module: {
      rules: [PUG, JS, CSS, ASSETS],
    },
    plugins: [
      ...Object.keys(HTMLS).map(
        (x) =>
          new HtmlWebpackPlugin({
            filename: `${x}.html`,
            template: `src/pug/${x}.pug`,
            inject: 'body',
          })
      ),
      new BeautifyHtmlWebpackPlugin(),
      new MiniCssExtractPlugin({
        filename: '[name].[hash].css',
      }),
    ],
  };
};
module.exports = (env, argv) => config(argv.mode);

